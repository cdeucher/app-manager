# docker build -t cli:0.1 . 
# docker run --rm -v $(pwd):/usr/src/app cli:0.1 run build


FROM node:latest

RUN mkdir -p /usr/src/app
RUN apt update -y

RUN npm install -g @vue/cli @vue/cli-service @vue/cli-plugin-babel @vue/cli-plugin-eslint @vue/babel-preset-app babel-eslint eslint eslint-plugin-vue vue-template-compiler
#ENTRYPOINT ["node"]
